#include "Vegetal.hpp"
#include <vector>
#include <memory>

class Terrain {
    public:  
        Terrain();
        void ajouterAuFichier(const Vegetal& type);
        void ajouterVegetal(std::unique_ptr<Vegetal> vegetal);
        void faireCroitreTout();
        void ChargerVegetauxDuFichier();
        int getNombreDeVegetaux() const;
        std::vector<std::unique_ptr<Vegetal>> getVegetaux();



    private: 
        std::vector<std::unique_ptr<Vegetal>> vegetaux;



    

};