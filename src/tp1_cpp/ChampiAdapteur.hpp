
#pragma once
#include "Champignon.hpp"
#include "Vegetal.hpp"

// NE PAS MODIFIER

class ChampiAdapteur : private Champignon, public Vegetal
{
	// NE PAS MODIFIER
	public:

		ChampiAdapteur(float vitessePousse);
        float getTaille() const override;
        void croissance() override;
        std::string getName() const  override;
		void setMeteo(Meteo meteo);

    private:
        Meteo mMeteo;

	/*	
		enum class Meteo { Soleil, PasSoleil };

		Champignon(float vitessePousse);

		float getHauteurChampignon() const;
		float getSurfaceChampignon() const;

		void pousse(Meteo meteoDuJour);

	// NE PAS MODIFIER
	private:
		float mHauteurChampignon = 0.0f;
		float mSurfaceChampignon = 0.0f;
		float mVitessePousse;

		*/
};

