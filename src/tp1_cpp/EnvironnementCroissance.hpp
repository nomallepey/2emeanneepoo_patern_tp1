
#pragma once
#include <string>

class EnvironnementCroissance
{
    public:
        virtual std::string getName() const = 0;
        virtual float getCoef() = 0;
        virtual ~EnvironnementCroissance() {}
};