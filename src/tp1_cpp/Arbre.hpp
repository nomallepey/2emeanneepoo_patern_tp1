
#include "Vegetal.hpp"
#include <string>
#include "EnvironnementCroissance.hpp"
#pragma once

class Arbre: public Vegetal
{
    public:
        Arbre();
        float getTaille() const override;
        void croissance() override;
        std::string getName() const  override;
        std::string getNameEnvironnementCroissance();
        void setEnvironnementCroissance(EnvironnementCroissance& env);

    protected:
        float mtaille;

    private:
        EnvironnementCroissance* mEnv;
        float getCoefEnvironnementCroissance();
};
