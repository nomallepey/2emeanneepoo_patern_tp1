
#include "ChampiAdapteur.hpp"


ChampiAdapteur::ChampiAdapteur(float vitessePousse) : Champignon(vitessePousse)
{
	setMeteo(Champignon::Meteo::Soleil);
}

void ChampiAdapteur::setMeteo(Champignon::Meteo meteo){
	mMeteo = meteo;
}

float ChampiAdapteur::getTaille() const{
	return getHauteurChampignon();
}

std::string ChampiAdapteur::getName() const{
	return "Champignon";
}

void ChampiAdapteur::croissance(){
	pousse(mMeteo);
}
       
