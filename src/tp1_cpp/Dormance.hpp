
#pragma once
#include "EnvironnementCroissance.hpp"
#include <string>

class Dormance : public EnvironnementCroissance
{
    public:
        Dormance();
        std::string getName() const override;
        float getCoef() override;
    private:
        const std::string name = "Dormance";
        const float coef = 0.05f;
};