
#pragma once
#include "EnvironnementCroissance.hpp"
#include <string>

class CroissanceEstivale : public EnvironnementCroissance
{
    public:
        CroissanceEstivale();
        std::string getName() const override;
        float getCoef() override;
    private:
        const std::string name = "CroissanceEstivale";
        const float coef = 1.50f;
};