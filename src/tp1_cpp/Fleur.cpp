
#include "Fleur.hpp"

#include <iostream>
Fleur::Fleur(float tailleMax):mtailleMax(tailleMax)
{
    mtailleMax = tailleMax;
};

float Fleur::getTaille() const{
    return mtaille;
}

void Fleur::croissance() {
    if (mtaille + 0.1f<= mtailleMax)
        mtaille = mtaille + 0.1f;
    else mtaille =  mtailleMax;
}

std::string  Fleur::getName() const {
    return "Fleur["+  std::to_string(mtailleMax) +"]" ;
}