
#pragma once

#include "Vegetal.hpp"
#pragma once

class Fleur: public Vegetal
{
    public:
        Fleur(float tailleMax);
        float getTaille() const override;
        void croissance() override;
        std::string getName() const  override;
        

    protected:
        float mtaille = 0;

    private:
        float mtailleMax;
};
