
#pragma once
#include "EnvironnementCroissance.hpp"
#include <string>

class CroissancePrintaniere : public EnvironnementCroissance
{
    public:
        CroissancePrintaniere();
        std::string getName() const override;
        float getCoef() override;
    private:
        const std::string name = "CroissancePrintanière";
        const float coef = 2.0f;
};