#include "Terrain.hpp"

#include <fstream>
#include <iostream>
#include "Arbre.hpp"
#include "Fleur.hpp"
#include <sstream>

Terrain::Terrain():vegetaux(){
}

std::vector<std::unique_ptr<Vegetal>> Terrain::getVegetaux(){
    return std::move(vegetaux);
}


void Terrain::faireCroitreTout(){
    for (const std::unique_ptr<Vegetal> & v: vegetaux){
        v->croissance();
    }
}

int Terrain::getNombreDeVegetaux() const{
    return vegetaux.size();
}

void Terrain::ajouterVegetal(std::unique_ptr<Vegetal> vegetal){
    vegetaux.push_back(std::move(vegetal));
}

// Fonction pour parser une ligne
std::pair<std::string, float> parserLigne(const std::string& ligne) {
    std::pair<std::string, float> resultat;
    size_t debutArbre = ligne.find("Arbre");
    size_t debutFleur = ligne.find("Fleur");
    if (debutArbre!= std::string::npos){
        std::string element = "Arbre";
        resultat.first = element;
    }
    else if (debutFleur!= std::string::npos){
        size_t debutCrochet = ligne.find('[');
        size_t finCrochet = ligne.find(']');
        std::string element = ligne.substr(0, debutCrochet);
        std::string nombreStr = ligne.substr(debutCrochet + 1, finCrochet - debutCrochet - 1);
        resultat.second = std::stof(nombreStr); // Conversion string vers int
        resultat.first = element;
    }

    
    
    return resultat;
}

void Terrain::ChargerVegetauxDuFichier(){
    std::ifstream fichier("../src/tp1_cpp/stockVegetaux.txt");
    if (!fichier.is_open()) {
        std::cerr << "Erreur lors de l'ouverture du fichier." << std::endl;
        return;
    }


    std::string ligne;
    while (std::getline(fichier, ligne)){
          std::pair<std::string, int> resultat = parserLigne(ligne);
            if (!resultat.first.empty()) {
                std::cout << "Vegetal : " << resultat.first << ", taille max: " << resultat.second << std::endl;
                        if (resultat.first == "Arbre") {
                            ajouterVegetal(std::make_unique<Arbre>());
                        }
                        else{
                            ajouterVegetal(std::make_unique<Fleur>(resultat.second));
                        }
            }

        }
        fichier.close();
}



// a changer pour creer fichier
void Terrain::ajouterAuFichier(const Vegetal& type) {
    std::ofstream outfile;

    outfile.open("stockVegetaux.txt", std::ios_base::app);
       
    if (!outfile.is_open()) {
        std::cerr << "Error opening file!" << std::endl;
    }

    outfile << type.getName()+ "\n";

    outfile.close();
    std::cout << "Text added to file successfully!" << std::endl;
}

// charger vegetaux depuis fichier
//fairecroitretout
// ajouterVegeatl