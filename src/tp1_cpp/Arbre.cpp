
#include "Arbre.hpp"
#include <iostream>
#include "EnvironnementCroissance.hpp"

Arbre::Arbre() : mtaille(0), mEnv(nullptr){};

float Arbre::getTaille() const {
    return mtaille;

}

void Arbre::croissance() {
    if (mEnv!=nullptr)
        mtaille = mtaille + mEnv->getCoef();
    else
        mtaille = mtaille + 1.0f;
}

std::string Arbre::getNameEnvironnementCroissance(){
    if (mEnv != nullptr){
        return mEnv->getName();
    }
    else return "";
}

void Arbre::setEnvironnementCroissance(EnvironnementCroissance& env){
    mEnv = &env;
}

std::string  Arbre::getName() const {
    return "Arbre";
}