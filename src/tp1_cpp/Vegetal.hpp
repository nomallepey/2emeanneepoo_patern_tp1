
#pragma once
#include <string>

class Vegetal
{
    public:
        Vegetal();
        virtual ~Vegetal() = default;
        virtual float getTaille() const;
        virtual std::string getName() const = 0;
        virtual void croissance() = 0;

    private:
        float mtaille;
};