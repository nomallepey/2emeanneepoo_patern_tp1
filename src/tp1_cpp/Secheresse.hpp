
#pragma once
#include "EnvironnementCroissance.hpp"
#include <string>

class Secheresse : public EnvironnementCroissance
{
    public:
        Secheresse();
        std::string getName() const override;
        float getCoef() override;
    private:
        const std::string name = "Secheresse";
        const float coef = 0.50f;
};